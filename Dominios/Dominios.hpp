#include <string>
#include <iostream>
#include <stdexcept>

using namespace std;

class Assento
{
private:
	char assento;

public:
	void set_assento(char);
	char get_assento()
	{
		return assento;
	}
	void validar(char) throw(invalid_argument);

};

class Bagagem
{
private:
	int bagagem;

public:
	void set_bagagem(int);
	int get_bagagem()
	{
		return bagagem;
	}
	void validar(int) throw(invalid_argument);

};

class Codigo_de_Banco
{
private:
	string banco;

public:
	void set_banco(string);
	string get_banco()
	{
		return banco;
	}
	void validar(string) throw(invalid_argument);
};

class Codigo_de_Carona
{
private:
	string carona;

private:
	void set_carona(string);
	string get_carona()
	{
		return carona;
	}
	void validar(string) throw(invalid_argument);
};

class Codigo_de_Reserva
{
private:
	string reserva;

public:
	void set_reserva(string);
	string get_reserva()
	{
		return reserva;
	}
	void validar(string) throw(invalid_argument);
};

class Cidade
{

private:
	string cidade;
public:
	void set_cidade(string);
	string get_cidade()
	{
		return cidade;
	}
	void validar(string) throw(invalid_argument);
};

class CPF
{
private:
	string cpf;
public:
	void set_cpf(string);
	string get_cpf()
	{
		return cpf;
	}
	void validar(string) throw(invalid_argument);
};

class Data
{
private:
	string data;

public:
	void set_data(string);
	string get_dataa()
	{
		return data;
	}
	void validar(string) throw(invalid_argument);
};

class Duracao
{
private:
	float duracao;

public:
	void set_duracao(float);
	float get_duracao()
	{
		return duracao;
	}
	void validar(float) throw(invalid_argument);
};

class Estado
{
private:
	string estado;

public:
	void set_estado(string);
	string get_estado()
	{
		return estado;
	}
	void validar(string) throw(invalid_argument);
};

class Email
{
private:
	string email;

public:
	void set_email(string);
	string get_email()
	{
		return email;
	}
	void validar(string) throw(invalid_argument);
};

class Nome
{
private:
	string nome;

public:
	void set_nome(string);
	string get_nome()
	{
		return nome;
	}
	void validar(string) throw(invalid_argument);
};

class Numero_Agencia
{
private:
	string agencia;
public:
	void set_agencia(string);
	string get_agencia()
	{
		return agencia;
	}
	void validar(string) throw(invalid_argument);
};

class Numero_Conta
{
private:
	string conta;
public:
	void set_conta(string);
	string get_conta(){
		return conta;
	}
	void validar(string) throw(invalid_argument);
};

class Preco
{
private:
	float preco;
public:
	void set_preco(float);
	float get_preco()
	{
		return preco;
	}
	void validar(float) throw(invalid_argument);
};

class Telefone
{
private:
	string telefone;
public:
	void set_telefone(string);
	string get_telefone()
	{
		return telefone;
	}
	void validar(string) throw(invalid_argument);
};

class Senha
{
private:
	string senha;
public:
	void set_senha(string);
	string get_senha()
	{
		return senha;
	}
	void validar(string senha) throw(invalid_argument);
};


class Vagas
{
private:
	int vagas;
public:
	void set_vagas(int);
	int get_vagas()
	{
		return vagas;
	}
	void validar(int vagas) throw(invalid_argument);
};
