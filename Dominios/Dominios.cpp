#include "Dominios.hpp"
#include <iostream>
#include <stdexcept>
#include <string>

using namespace std;

void Assento::set_assento(char valor)
{
	Assento::validar(valor);

	this->assento = valor;
}

void Assento::validar(char valor) throw(invalid_argument)
{
	if (valor != 'D' && valor != 'T')
		throw invalid_argument("formato informado do assento nao confere com o especificado");
}

void Bagagem::set_bagagem(int valor)
{
	Bagagem::validar(valor);

	this->bagagem = valor;
}

void Bagagem::validar(int valor) throw(invalid_argument)
{
	if(valor > 4 || valor < 0)
		throw invalid_argument("quantidade indisponivel de bagagem(ns)");
}

void Codigo_de_Banco::set_banco(string valor) 
{
	Codigo_de_Banco::validar(valor);

	this->banco = valor;
}

void Codigo_de_Banco::validar(string valor) throw(invalid_argument)
{
	if(valor.length() != 3)
		throw invalid_argument("formato invalido para codigo de banco");

	for(int i = 0; i < valor.length(); i++)
	{
		if(isdigit(valor[i]) == false)
			throw invalid_argument("formato invalido para codigo de banco");	
	}
}

void Codigo_de_Carona::set_carona(string valor)
{
 	Codigo_de_Carona::validar(valor);

 	this->carona = valor;
}

void Codigo_de_Carona::validar(string valor) throw(invalid_argument)
{
	if(valor.length() != 4)
		throw invalid_argument("formato invalido para codigo de carona");

	for(int i = 0; i < valor.length(); i++)
	{
		if(isdigit(valor[i]) == false)
			throw invalid_argument("formato invalido para codigo de carona");	
	}
}
void Codigo_de_Reserva::set_reserva(string valor)
{
	Codigo_de_Reserva::validar(valor);

	this->reserva = valor;
}

void Codigo_de_Reserva::validar(string valor) throw(invalid_argument)
{
	if (valor.length() != 5)
		throw invalid_argument("formato invalido para codigo de reserva");

	for(int i = 0; i < valor.length(); i++)
	{
		if(isdigit(valor[i]) == false)
			throw invalid_argument("formato invalido para codigo de reserva");	
	}
	
}

void Cidade::set_cidade(string valor) 
{
	Cidade::validar(valor);

	this->cidade = valor;
}
void Cidade::validar(string valor) throw(invalid_argument)
{
	if(valor.length()>10)
		throw invalid_argument("formato inválido para cidade, digite novamente");
}

void CPF::set_cpf(string valor)
{
	CPF::validar(valor);

	this->cpf = valor;
}

void CPF::validar(string valor) throw(invalid_argument)
{
	if(valor.length() != 14)
		throw invalid_argument("formato invalido para CPF, digite novamente");

	if(valor[3]!='.' ||  valor[7]!='.' || valor[11]!='-')
		throw invalid_argument("formato invalido para CPF, digite novamente com - e .");
	

}

void Data::set_data(string valor)
{
	Data::validar(valor);

	this-> data = valor;
}

void Data::validar(string valor) throw(invalid_argument)
{
	if(valor.length()!= 8)
		invalid_argument("formato invalido para data");
	
	if(valor[2] != '/' || valor[5] != '/')
		throw invalid_argument("formato invalido para data");
	
	for(int i = 0; i< valor.length(); i++)
	{
		if(i == 2 || i == 5)
			i++;
		if(!isdigit(valor[i]))
			throw invalid_argument("formato invalido para data");
	}

	int dia = atoi(valor.substr(0,2).c_str());
    int mes = atoi(valor.substr(3,2).c_str());
    int ano = atoi(valor.substr(6,2).c_str());

    if(mes == 1 || mes == 3 || mes == 5 || mes == 7 || mes == 8 || mes == 10 || mes == 12){
        if(dia<1 || dia>31){
            throw invalid_argument("valor invalido");
        }
    }
    else if(mes == 4 || mes == 6 || mes == 9 || mes == 11)
	{
        if(dia<1 || dia>30)
            throw invalid_argument("valor invalido");
    }
    else if(mes == 2)
	{
        /*Como ano deve estar entre 2000 e 2099, o c�lculo do ano bissexto pode ser simplificado
        para apenas a congru�ncia com 4*/
        if(ano%4 == 0)
		{ // se for bissexto
            if(dia<1 || dia>29)
                throw invalid_argument("valor invalido");
        }
        else
		{
            if(dia<1 || dia>28)
                throw invalid_argument("valor invalido");
        }
    }
    else throw invalid_argument("valor invalido"); //mes invalido

	
}

//void Duracao::set_duracao

void Estado::set_estado(string valor)
{
	Estado::validar(valor);

	this->estado = valor;
}

void Estado::validar(string valor) throw(invalid_argument)
{
	if(valor!="AC" && valor!="AL" && valor!="AP" && valor!="AM" && valor!="BA" &&
       valor!="CE" && valor!="DF" && valor!="ES" && valor!="GO" && valor!="MA" &&
       valor!="MT" && valor!="MS" && valor!="MG" && valor!="PA" && valor!="PB" &&
       valor!="PR" && valor!="PE" && valor!="PI" && valor!="RJ" && valor!="RN" &&
       valor!="RS" && valor!="RO" && valor!="RR" && valor!="SC" && valor!="SP" &&
       valor!="SE" && valor!="TO")

        throw invalid_argument("formato invalido para Estado");
}

void Email::set_email(string valor)
{
	Email::validar(valor);

	this->email = valor;	
}

void Email::validar(string valor) throw(invalid_argument)
{
	//if(valor.find('@') == -1)
}

void Nome::set_nome(string valor)
{
	Nome::validar(valor);

	this->nome = valor;
}

void Nome::validar(string valor) throw(invalid_argument)
{

}

void Numero_Agencia::set_agencia(string valor)
{
	Numero_Agencia::validar(valor);

	this->agencia = valor;
}

void Numero_Agencia::validar(string valor) throw(invalid_argument)
{

}

void Numero_Conta::set_conta(string valor)
{
	Numero_Conta::validar(valor);

	this->conta = valor;
}
void Numero_Conta::validar(string valor) throw(invalid_argument)
{

}
void Preco::set_preco(float valor)
{
	Preco::validar(valor);

	this->preco = valor;
}

void Preco::validar(float valor) throw(invalid_argument)
{

}

void Telefone::set_telefone(string valor)
{
	Telefone::validar(valor);

	this->telefone = valor;
}

void Telefone::validar(string valor) throw(invalid_argument)
{
	
}