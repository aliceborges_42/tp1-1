#include <iostream>
#include"Dominios.hpp"
#include <stdexcept>

using namespace std;

//Entidade
class Usuario
{
private:
	Nome nome;
	Telefone telefone;
	Email email;
	Senha senha;
	CPF cpf;
public:
    void set_usuario(Usuario*)
	//void set_nome(string valor);
	string get_nome()
	{
		return nome.get_nome();
	}

	//void set_telefone(int valor);
	int get_telefone()
	{
		return telefone.get_telefone();
	}

	//void set_email(string valor);
	string get_email()
	{
		return email.get_email();
	}

	//void set_senha(string valor);
	string get_senha()
	{
	    return senha.get_senha();
    }

	//void set_cpf(int *valor);
	int get_cpf()
	{
		return cpf.get_cpf();
	}

};

// ***************************************************
// Dominios
class Reserva
{
private:
	Codigo_de_Reserva codigo;
	Assento assento;
	Bagagem bagagem;
public:
	void set_reserva(int valor);
	int get_codigo()
	{
		return codigo.get_reserva();
	}

	//void set_assento(char valor);
	char get_assento()
	{
		return assento.get_assento();
	}

	//void set_bagagem(int valor);
	int get_bagagem()
	{
		return codigo.get_bagagem();
	}


};

class Carona
{
private:
	Codigo_de_Carona codigo;
	Cidade cidade_origem;
	Cidade cidade_destino;
	Estado estado_origem;
	Estado estado_destino;
	Data data;
	Duracao duracao;
	Vagas vagas;
	Preco preco;
public:
	void set_carona(int valor);
	int get_codigo()
	{
		return codigo.get_carona();
	}

	//void set_cidade_origem(string valor);
	string get_cidade_origem()
	{
		cidade_origem.get_cidade();
	}

	//void set_cidade_destino(string valor);
	string get_cidade_destino()
	{
		cidade_destino.get_cidade();
	}

	//void set_estado_origem(string valor);
	string get_estado_origem()
	{
		return estado_origem.get_estado();
	}

	//void set_estado_destino(string valor);
	string get_estado_destino()
	{
		return estado_destino.get_estado();
	}

	//void set_data(string valor);
	string get_data()
	{
		return data.get_data();
	}

	//void set_duracao(float valor);
	float get_duracao()
	{
		return duracao.get_duracao();
	}

	//void set_vagas(int valor);
	int get_vagas()
	{
		vagas.get_vagas();
	}

	//void set_preco(float valor);
	float get_preco()
	{
		return preco.get_preco();
	}
};

class Conta
{
private:
	Codigo_de_Banco banco;
	Numero_Agencia agencia;
	Numero_Conta numero;
public:
	void set_conta(int valor);
	int get_banco()
	{
		return banco.get_banco();
	}

	//void set_agencia(int valor);
	int get_agencia()
	{
		return agencia.get_agencia();
	}

	//void set_numero(int valor);
	int get_numero()
	{
		numero.get_conta();
	}

};
