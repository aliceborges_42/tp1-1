#include<iostream>
#include "Dominios.hpp"

using namespace std;

int main()
{
    Assento ass;
    try
    {
        ass.validar('D');
        cout<<"Deu Bom"<<endl;
        ass.set_assento('D');
    }  
    catch(invalid_argument z)
    {
        cout<<"Deu Ruim "<<z.what()<<endl;
    } 

    Bagagem x;
    try
    {
        x.validar(2);
        cout<<"Deu Bom"<<endl;
        x.set_bagagem(2);
    }  
    catch(invalid_argument v)
    {
        cout<<"Deu Ruim "<<v.what()<<endl;
    }
    int c = x.get_bagagem();
    cout<< c ;
    return 0; 
}
